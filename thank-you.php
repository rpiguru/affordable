<?php include('header.php'); ?>

  <div class="contain">
    <div class="clearfix p5"></div>
    <div id="opening-blurb" class="tac pt45 pb55">
      <section>
        <h4 class="pt100 pb100 m0a">
          Thank you for your inquiry, we will be in touch shortly.<br />
          <a href="/" class="underline">go back home</a>
        </h4>
      </section>
    </div>
    
    <div class="contain pt45 pb75">
      <div class="tac w100">
        <a href="https://a806-housingconnect.nyc.gov/nyclottery/lottery.html#current-projects" class="button redBG" target="_blank" onClick="ga('send', 'event', 'Lead', 'Apply - Thank You');">Apply Now</a>
      </div>      
    </div>
        
  </div>

<?php include('footer.php'); ?>