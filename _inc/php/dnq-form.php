<?php
include 'util.php';

if(!isset($_POST['submit']))
{
	//This page should not be accessed directly. Need to submit the form.
	echo "error; you need to submit the form!";
}
$name = $_POST['name'];
$visitor_email = $_POST['email'];
$visitor_phone = $_POST['phone'];
$message = $_POST['message'];
$householdSize = $_POST['dnq-household-size'];
$householdIncome = $_POST['dnq-household-income'];

//Validate first
if(empty($name)||empty($visitor_email)) 
{
    echo "Name and email are mandatory!";
    exit;
}

if(IsInjected($visitor_email))
{
    echo "Please use alternative email!";
    exit;
}

$email_from = "noreply@535carlton.com";
$email_subject = "535 Carlton - Unqualified Applicant: $name";
$email_body = "<br /><h1 style='color:#C92418'>Unqualified Applicant</h1><p>This person filled out the Do You Qualify quiz on 535carlton.com and <span style='font-style:italic;font-weight:bold;color:#C91F1A'>failed</span>. Below is their information and the choices they made.</p><br /><table border='1' cellpadding='10'><thead style='background:#716558;color:#fff' bgcolor='#716558'><th>Full Name</th><th>Email</th><th>Phone</th><th>Household Size</th><th>Household Income</th><th>Message</th></thead><tbody>".
  "<td>$name</td>".
  "<td>$visitor_email</td>".
  "<td>$visitor_phone</td>".
  "<td align='center'>$householdSize</td>".
  "<td align='center'>$householdIncome</td>".
  "<td>$message</td></tbody></table>";

$to = "wt@silvercreativegroup.com";

$headers = "From: $email_from \r\n";
$headers .= "Reply-To: $visitor_email \r\n";
// $headers .= "CC: test@example.com\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

//Send the email!
mail($to,$email_subject,$email_body,$headers);

// Push to the google sheet
$_POST['type'] = 'dnq';

if ($_POST['dnq-household-size'] == null){
    $_POST['dnq-household-size'] = 0;
}
if ($_POST['dnq-household-income'] == null){
    $_POST['dnq-household-income'] = 0;
}

push_google_sheet($_POST);

//done. redirect to thank-you page.
header('Location: /thank-you');

// Function to validate against any email injection attempts
function IsInjected($str)
{
  $injections = array('(\n+)',
              '(\r+)',
              '(\t+)',
              '(%0A+)',
              '(%0D+)',
              '(%08+)',
              '(%09+)'
              );
  $inject = join('|', $injections);
  $inject = "/$inject/i";
  if(preg_match($inject,$str))
    {
    return true;
  }
  else
    {
    return false;
  }
}
   
?>