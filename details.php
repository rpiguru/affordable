<?php include('header.php'); ?>

  <div class="contain">
    <div class="clearfix p100"></div>
<!--
    <div id="opening-blurb" class="tac pt45 pb55">
      <section>
        <h4>
          All of these apartments will be subject to a lottery process managed by the<br /><span>New York City Housing Development Corporation</span>
        </h4>
      </section>
    </div>
-->
    
    <div id="opening-blurb" class="tac pt45 pb55">
      <section>
        <h2>
          Located on 11 subway lines<br />and the LIRR<br />
        </h2>
        <div class="subway-lines pt10">
          <span id="sw-2" class="sw-red">2</span>
          <span id="sw-3" class="sw-red">3</span>
          <span id="sw-4" class="sw-green">4</span>
          <span id="sw-5" class="sw-green">5</span>
          <span id="sw-A" class="sw-blue">A</span>
          <span id="sw-C" class="sw-blue">C</span>
          <span id="sw-B" class="sw-orange">B</span>
          <span id="sw-D" class="sw-orange">D</span>
          <span id="sw-G" class="sw-l-green">G</span>
          <span id="sw-R" class="sw-yellow">R</span>
          <span id="sw-Q" class="sw-yellow">Q</span>
        </div>
      </section>
    </div>

    <div class="tac pb25 pt25">
<!--       <img src="_inc/img/535Table.jpg" alt="" width="100%" height="auto" class="m0a" /> -->
      
      <div class="w100 clearfix pb50">
        <div class="w50 left">
          <h2>Features</h2><br />
          <ul class="fontLarge">
          	<li>Video Intercom Access</li>
          	<li>European Oak Wood Flooring</li>
          	<li>Stone Countertops</li>
          	<li>Whirlpool Appliances, Including Dishwasher and Microwave</li>
          	<li>Moen Bathroom Fixtures</li>
          	<li>In-unit Temperature Control</li>
          </ul>
        </div>
        <div class="w50 left">
          <h2>Amenities</h2><br />
          <ul class="fontLarge">
          	<li>Fitness Center*</li>
          	<li>Children’s Playroom*</li>
          	<li>Residents’ Lounge*</li>
          	<li>Residents’ Terrace with Community Garden</li>
          	<li>Bike Room*</li>
          	<li>Laundry on Each Floor</li>
          	<li>*Additional fees apply</li>
          </ul>
        </div>
      </div>
      
    </div>
<!--
    <div id="calendar" class="clearfix">
      <h2 class="tac pt25 pb15">Where To Learn More</h2>
      <div id="events-container">
        <div id="events" class="table w100">
        <div class="tableRow event">
          <div class="tableCell vat p10">
            <div class="date">
              17<br /><span>Aug</span>
            </div>
          </div>
          <div class="tableCell vat p15">
            <h4>
              Affordable Housing Information Session
            </h4>
            <i class="fa fa-clock-o"></i> 
              6:30 PM
            <address class="inline-block pl10">
            <i class="fa fa-map-marker"></i>
              NYU Tandon School of Engineering, 5 MetroTech Center, Pfizer Auditorium, Brooklyn, NY 11201
            </address>
            <p class="pt5">
              Learn more about 535 Carlton and the affordable housing lottery. To attend please RSVP to <a href="mailto:535Carlton@mutualhousingny.org">535Carlton@mutualhousingny.org</a> or call <a href="tel:7182468080,224">718.246.8080 ext 224</a>
            </p>
          </div>
        </div>
        <div class="tableRow event">
          <div class="tableCell vat p10">
            <div class="date">
              13<br /><span>Sep</span>
            </div>
          </div>
          <div class="tableCell vat p15">
            <h4>
              Affordable Housing Information Session
            </h4>
            <i class="fa fa-clock-o"></i> 
              6:30 PM
            <address class="inline-block pl10">
            <i class="fa fa-map-marker"></i>
              PS 9, 80 Underhill Avenue, Brooklyn, NY
            </address>
            <p class="pt5">
              Learn more about 535 Carlton and the affordable housing lottery. To attend please RSVP to <a href="mailto:535Carlton@mutualhousingny.org">535Carlton@mutualhousingny.org</a> or call <a href="tel:7182468080,224">718.246.8080 ext 224</a>
            </p>
          </div>
        </div>
        <!-- EVENT 
        <div class="tableRow event">
          <div class="tableCell vat p10">
            <div class="date">
              10<br /><span>May</span>
            </div>
          </div>
          <div class="tableCell vat p15">
            <h4>
              Affordable Housing Information Session
            </h4>
            <i class="fa fa-clock-o"></i> 
              06:30 PM
            <address class="inline-block pl10">
            <i class="fa fa-map-marker"></i>
              BAM Rose Cinema #4; 30 Lafayette Avenue, Brooklyn, NY 11217
            </address>
            <p class="pt5">
              Learn more about 461 Dean and the affordable housing lottery. To attend please RSVP to <a href="mailto:461dean@mutualhousingny.org">461dean@mutualhousingny.org</a> or call <a href="tel:17182468080,224">718.246.8080 ext 224</a>
            </p>
          </div>
        </div>-->
        <!-- EVENT 
        <div class="tableRow event">
          <div class="tableCell vat p10">
            <div class="date">
              11<br /><span>May</span>
            </div>
          </div>
          <div class="tableCell vat p15">
            <h4>
              461 Dean Announcement at Community Board 2 General Meeting
            </h4>
            <i class="fa fa-clock-o"></i> 
              06:00 PM
            <address class="inline-block pl10">
            <i class="fa fa-map-marker"></i>
              Ingersoll Community Center Gymnasium, 177 Myrtle Avenue, Brooklyn, NY 11201
            </address>
            <p class="pt5">
              Brief Presentation on 461 Dean affordable housing lottery toward the beginning of the community board agenda.
            </p>
          </div>
        </div>
        -->
        <!-- EVENT 
        <div class="tableRow event">
          <div class="tableCell vat p10">
            <div class="date">
              12<br /><span>May</span>
            </div>
          </div>
          <div class="tableCell vat p15">
            <h4>
              461 Dean Announcement at Community Board 8 General Meeting
            </h4>
            <i class="fa fa-clock-o"></i> 
              07:00 PM
            <address class="inline-block pl10">
            <i class="fa fa-map-marker"></i>
              Center Light Health Care Center (CNR); 727 Classon Avenue, Brooklyn, NY 11238
            </address>
            <p class="pt5">
              Brief Presentation on 461 Dean affordable housing lottery toward the beginning of the community board agenda.
            </p>
          </div>
        </div>
        -->
        <!-- EVENT
        <div class="tableRow event">
          <div class="tableCell vat p10">
            <div class="date">
              16<br /><span>May</span>
            </div>
          </div>
          <div class="tableCell vat p15">
            <h4>
              Affordable Housing Information Session
            </h4>
            <i class="fa fa-clock-o"></i> 
              06:00 PM
            <address class="inline-block pl10">
            <i class="fa fa-map-marker"></i>
              Bed Stuy Restoration; 1368 Fulton Street, Brooklyn, NY 11216
            </address>
            <p class="pt5">
              Learn more about 461 Dean and the affordable housing lottery. To attend please RSVP to <a href="mailto:461dean@mutualhousingny.org">461dean@mutualhousingny.org</a> or call <a href="tel:17182468080,224">718.246.8080 ext 224</a>
            </p>
          </div>
        </div> -->
        <!-- EVENT
        <div class="tableRow event">
          <div class="tableCell vat p10">
            <div class="date">
              24<br /><span>May</span>
            </div>
          </div>
          <div class="tableCell vat p15">
            <h4>
              Affordable Housing Information Session
            </h4>
            <i class="fa fa-clock-o"></i> 
              06:30 PM
            <address class="inline-block pl10">
            <i class="fa fa-map-marker"></i>
              PS 9, 80 Underhill Avenue Brooklyn NY, 11238
            </address>
            <p class="pt5">
              Learn more about 461 Dean and the affordable housing lottery. To attend please RSVP to <a href="mailto:461dean@mutualhousingny.org">461dean@mutualhousingny.org</a> or call <a href="tel:17182468080,224">718.246.8080 ext 224</a>
            </p>
          </div>
        </div>
      </div>
      </div>
    </div>
    
    <div class="contain pt45 pb25">
      <div class="tac w100">
        <a href="https://a806-housingconnect.nyc.gov/nyclottery/lottery.html#current-projects" target="_blank" class="button redBG" onClick="ga('send', 'event', 'Lead', 'Apply - After Events');">Apply Now</a>
      </div>      
    </div>
    -->
    
    <h2 class="tac pt55 faqs">Frequently Asked Questions</h2>
    <div id="accordions" data-accordion-group class="pt15 pb100">
      
      <div data-accordion class="accordion">
        <div data-control>What constitutes income?</div>
        <div data-content>
          <div>
            <p>
              Income is from all sources from anyone over the age of 18 in your household. This includes wages, bonuses, tips, unemployment compensation, alimony, child support, pension benefits, Social Security benefits, recurring gifts and public assistance. 
            </p>
          </div>
        </div>
      </div>
      
      <div data-accordion class="accordion">
        <div data-control>What constitutes household number?</div>
        <div data-content>
          <div>
            <p>
              All individuals who will be living with you at 535 Carlton, including children, spouse, partner, siblings, parents, and current roommates. Please note that for roommates to be eligible they must currently be living together and provide proof in the form of a signed lease or mail. 
            </p>
          </div>
        </div>
      </div>
      
      <div data-accordion class="accordion">
        <div data-control>What documentation is needed for the application process?</div>
        <div data-content>
          <div>
            <p>
              Applicants will be required to provide documentation verifying their income and qualification.<br />
              <a href="./_inc/img/535Carlton_OpenHouseChecklist_July17.pdf" target="_blank">Download Document List</a>
            </p>
          </div>
        </div>
      </div>
      
      <div data-accordion class="accordion">
        <div data-control>How long will the application process take?</div>
        <div data-content>
          <div>
            <p>
              The entire process includes viewing the building and units, submitting all documentation during an appointment, and verification by HDC, and may take 6-10 weeks from the start to your move in date. 
            </p>
          </div>
        </div>
      </div>
      
    </div>
    
<!--
    <div class="contain pt45 pb75">
      <div class="tac w100">
        <a href="https://a806-housingconnect.nyc.gov/nyclottery/lottery.html#current-projects" target="_blank" class="button redBG" onClick="ga('send', 'event', 'Lead', 'Apply - After FAQs');">Apply Now</a>
      </div>      
    </div>
-->

    <div id="opening-blurb" class="tac pt45 pb55">
      <section>
        <h2>
          Documents required for application
        </h2>
      </section>
    </div>
    
    <div class="tac pb55 pt15">
      <img src="_inc/img/OH-1.jpg" alt="" width="100%" height="auto" class="m0a" />
      <img src="_inc/img/OH-2.jpg" alt="" width="100%" height="auto" class="m0a" />
<!--       <a href="./_inc/img/535Carlton_OpenHouseChecklist_July17.pdf" target="_blank" class="button">Download Open House Application</a> -->
    </div>
        
  </div>

<?php include('footer.php'); ?>